// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Block.generated.h"

UCLASS(Abstract)
class FPS_CPP_API ABlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlock();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Mesh, DisplayName = "Block mesh")
	UStaticMeshComponent* MeshBlock;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Variable, DisplayName = "Time to mine")
	float TimeToMine = 1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
