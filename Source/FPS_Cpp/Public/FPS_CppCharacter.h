// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "WorldGenerator.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Character.h"
#include "Logging/LogMacros.h"
#include "FPS_CppCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class UInputAction;
class UInputMappingContext;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

// TODO Add crouching (Height 95 cm to fit in 1 block gaps)
// TODO Add not falling off ledges when crouched
// TODO Rework Placing blocks to ETriggerEvent::Triggered with cooldown

UCLASS(config=Game)
class AFPS_CppCharacter : public ACharacter
{
	GENERATED_BODY()

	// UPROPERTIES ///////////////////////////////////////////////////////////////////////////////
	
	/** Box collider that will trigger extension of the world */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* WorldExtendCollider;
	
	/** AimBox mesh: is displayed where player can place blocks */
	UPROPERTY(VisibleAnywhere, Category = Mesh, DisplayName = "AimBox")
	UStaticMeshComponent* MeshAimBox;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;
	
	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	/** Mine Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MineAction;

	/** Place Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* PlaceAction;

	/** Next block type Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* NextBlockTypeAction;

	/** Previous block type Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* PreviousBlockTypeAction;

	/** Save World Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SaveWorldAction;

	/** Load World Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LoadWorldAction;

	/** Max distance at which a player can mine or place blocks */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	float MaxInteractDistance = 500;
	
	// PUBLIC METHODS //////////////////////////////////////////////////////////////////////////
public:
	AFPS_CppCharacter();
	
	virtual void Tick(float DeltaSeconds) override;
	
	/** Returns FirstPersonCameraComponent subobject */
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	// PROTECTED METHODS //////////////////////////////////////////////////////////////////////////
protected:
	virtual void BeginPlay() override;
	
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	/** Called for mining input */
	void Mine(const FInputActionValue& Value);

	/** Called for mining input */
	void StopMining(const FInputActionValue& Value);

	/** Called for placing blocks input */
	/** Place currently selected block at AimBoxPosition */
	void PlaceBlock(const FInputActionValue& Value);

	/** Called for input of selecting next block type for building */
	void NextBlockType(const FInputActionValue& Value);

	/** Called for input of selecting previous block type for building */
	void PreviousBlockType(const FInputActionValue& Value);

	/** Called for input of saving world */
	void SaveWorld(const FInputActionValue& Value);

	/** Called for input of loading world */
	void LoadWorld(const FInputActionValue& Value);

	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/** Update
	 * the block at which the user is currently looking within MaxInteractDistance,
	 * position for the AimBox that indicates where the player can build,
	 * and whether the position is valid */
	void UpdateAimBoxProperties(bool bDisplayAimBox = true);

	// PRIVATE VARIABLES //////////////////////////////////////////////////////////////////////////
private:
	/** World generator to inform of placed and mined blocks - Refactor using events? */
	UPROPERTY()
	UWorldGenerator* WorldGenerator;
	
	/** Save coordinates of block where user is looking (able to place blocks) */
	FVector AimBoxPosition;

	/** Indicates whether the player can place a block */
	bool bIsAimBoxPositionValid = false;

	bool bIsMining = false;

	/** Index of selected block type for building */
	int SelectedBlockType = 0;

	/** The block at which the player is currently looking within MaxInteractDistance */
	// UPROPERTY not needed since ABlock being destroyed during garbage collection should never bother us
	ABlock* LookedAtBlock = nullptr;
	
	/** The block that is currently being mined */
	// UPROPERTY not needed since ABlock being destroyed during garbage collection should never bother us
	ABlock* MinedBlock = nullptr;

	/** Remaining time for mining current block */
	float TimeToMine = 0;

	// PRIVATE METHODS //////////////////////////////////////////////////////////////////////////
private:
	// Setup scale of collision to detect when to generate new parts of the world
	void SetupWorldExtendDetectors();
	
	/** RayCast from camera with max distance of MaxInteractDistance */ 
	FHitResult RayCastForBuilding(bool bDrawDebugLine = false) const;

	/** Decrease Mine time by DeltaSeconds */
	void UpdateMiningStatus(const float DeltaSeconds);

	/** Resets value of mining variables */
	void ResetMiningStatus();

	/** Debug purposes - print text to screen */
	static void DebugScreenLog(const FString& Message);
};
