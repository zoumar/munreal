// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ChunkLib.h"
#include "ChunkBlocks.h"
#include "WorldGenerator.generated.h"

// TODO Set up in blueprint or something more clever than a magic numbers
#define RADIUS_TO_GENERATE 2
#define RADIUS_TO_SPAWN 1

class USavedWorld;
class AFPS_CppCharacter;
class AFPS_CppGameMode;
class FChunkLib;

DECLARE_LOG_CATEGORY_EXTERN(LogWorldGen, Log, All);

// TODO Refactor to Actor and distribute spawning blocks workload to AACtor::Tick method to only spawn blocks for several milliseconds
// TODO During such workload distribution do not spawn in columns of blocks, but rather first all surface blocks and then
// TODO   work your way down. Also possibly first spawn the Player chunk surface first on initial world generation  

/**
 *	Generates data of world, spawns its blocks and handles all
 */
UCLASS(config=Game)
class FPS_CPP_API UWorldGenerator : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY()
	USavedWorld* SavedData;

	// Methods /////////////////////////////////////////////////////////////////////////////////

	// Generate the blocky world
	void GenerateWorld();

	// React to player movement
	// Delete far away chunks and generate new ones or regenerate previously visited ones 
	void AdjustWorldToPlayerPosition(const FVector& PlayerPosition);

	// Spawn the world after loading it
	void SpawnLoadedWorld();
	
	// Delete actors from supplied Chunk. Keep generated data like Gradients and BlocksTopCoordinates
	void DeleteChunkBlocks(FIntVector2 ChunkCoords);
	void DeleteChunkBlocks(int ChunkIndex, TArray<FChunkBlocks>& InChunkBlocks);

	// Add Block to supplied Chunk; if bUser == true then block was placed by the user, otherwise during world generation
	// If the bRespawn == true, then the block was user block recreated after returning to chunk
	void AddBlockToChunk(ABlock* Block, int ChunkIndex, bool bUser = true, bool bRespawn = false);
	void AddBlockToChunk(ABlock* Block, FIntVector2 ChunkCoords, bool bUser = true);
	void AddBlockToChunk(ABlock* Block, bool bUser = true);
	void MineBlockFromChunk(ABlock* Block);

	// Save and load world state
	void SaveWorld(const AFPS_CppCharacter& Player);
	void LoadWorld(AFPS_CppCharacter& Player);

	TArray<FChunk>& GetChunks();
	
	// Premultiplied values by 100 to get Unreal centimeters; also type double to avoid unnecessary casting
	int GetPerlinHeightMultiplier() const;
	double GetPerlinHeightMultiplierCm() const;
	double GetSnowHeightCm() const;
	double GetHardenDepthCm() const;
	int GetChunkGranularity() const;
	
	// Precomputed size of chunk in one horizontal dimension in blocks
	int GetChunkOffset() const;
	int GetChunkOffsetCm() const;

private:
	// Member Variables ////////////////////////////////////////////////////////////////////////

	
	// TODO Optimize to more clever structures so that we avoid unnecessary searching for specific chunks

	// Data of generated chunks
	TArray<FChunk> Chunks;

	// Data of Block actors and mined blocks 
	TArray<FChunkBlocks> ChunkBlocks;

	// Transform of player for the purposes of save/load functionality
	FTransform PlayerTransform;
	
	// Ease of Use and slight optimization variables
	int PerlinHeightMultiplier;
	double PerlinHeightMultiplierCm;
	double SnowHeightCm;
	double HardenDepthCm;
	int ChunkGranularity;
	int ChunkOffset;
	int ChunkOffsetCm;

	FString SaveSlotName = "DefaultSaveSlot";

	// Saved GameMode for ease of access
	UPROPERTY()
	AFPS_CppGameMode* GameMode;

	// Methods /////////////////////////////////////////////////////////////////////////////////
	
	// Fills values of member variables based on supplied GameMode
	void InitMembers();
	UClass* GetBlockClass(const ABlock* Block) const;

	// Check if supplied data is fine; returns true if so, false otherwise
	bool ValidateInputsOfWorldGeneration() const;
	
	// Compute FVector positions of surface blocks for specified chunk using Perlin noise
	// Can be run asynchronously (therefore static)
	// Requires the memory in Chunks Array to be already allocated
	static void ComputeBlockHeights(UWorldGenerator& RWorldGenerator, int ChunkIndex,
	                                 FChunkLib::EInterpolationMode = FChunkLib::EIM_Linear);
	static void ComputeBlockHeights(UWorldGenerator& RWorldGenerator, FChunk& Chunk,
	                                 FChunkLib::EInterpolationMode InterpolationMode);
	static void ComputeBlockHeights(UWorldGenerator& RWorldGenerator, FChunk* Chunk,
	                                 FChunkLib::EInterpolationMode InterpolationMode);
	static void ComputeBlockHeights(UWorldGenerator& RWorldGenerator, int ChunkX, int ChunkY,
	                                 FChunkLib::EInterpolationMode InterpolationMode);

	// Spawn column of blocks in specified coordinates of specified chunk
	void SpawnBlockColumn(FChunk& Chunk, int X, int Y, int ChunkIndex = -1);
	void SpawnBlockColumn(FChunk* Chunk, int X, int Y, int ChunkIndex = -1);
	void SpawnBlockColumn(int ChunkIndex, int X, int Y);

	// Spawn all blocks for chunk with supplied index
	void SpawnChunkBlocks(FChunk& Chunk);
	void SpawnChunkBlocks(FChunk* Chunk);

	// Computes Gradients, Smoothing data, Block heights and spawns blocks for supplied Chunk array
	template<typename T>
	void GenerateChunks(TArray<T>& Array);
	template<typename T>
	void GenerateAndSpawnChunks(TArray<T>& Array);
	template<typename T>
	void SpawnChunks(TArray<T>& Array);
	
	// Determine which chunks should be generated and displayed based on Player's position
	// Compute ChunkStartCoordinates of chunks that are inside of given Radius (in number of Chunks) from given position
	TArray<FIntVector2> ComputeSurroundingChunkCoords(const FIntVector2& StartCoords, int Radius) const;
		
	// Allocates memory for chunks during map extension 
	TArray<FChunk*> AllocateChunksFromStartCoords(TArray<FIntVector2>& StartCoords);
};
