// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPS_CppGameMode.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "WorldExtendDetector.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogWorldExtendDetector, Log, All);

UCLASS()
class FPS_CPP_API AWorldExtendDetector : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AWorldExtendDetector();
	
	/** Root component to allow moving all colliders in unison */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* Root;

	/** Box collider that will trigger extension of the world to the north in positive Y axis */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* YPNorth;

	/** Box collider that will trigger extension of the world to the south in negative Y axis */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* YNSouth;

	/** Box collider that will trigger extension of the world to the east in positive X axis */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* XPWest;
	
	/** Box collider that will trigger extension of the world to the west in negative X axis */
	UPROPERTY(VisibleAnywhere, Category="Default")
	UBoxComponent* XNEast;

	UPROPERTY()
	UWorldGenerator* WorldGenerator;
	
	int ChunkGranularity;
	double BoundaryDistance;
	int ColliderHeight;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Methods ////////////////////////////////////////////////////////////////////////////////
	UFUNCTION()
	void OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void InitMembers(AFPS_CppGameMode& GameMode);
	void SetupBoundaries();
};
