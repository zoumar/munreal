// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "WorldGenerator.h"
#include "Block.h"
#include "GameFramework/GameModeBase.h"
#include "FPS_CppGameMode.generated.h"

class AWorldExtendDetector;

UCLASS(minimalapi)
class AFPS_CppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPS_CppGameMode();

	// Override to trigger world generation
	virtual void BeginPlay() override;

	// All types of blocks - pick in the Data Blueprint
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<TSubclassOf<ABlock>> BlockTypes;

	// How many blocks will be generated between each gradient vector in a chunk
	// Chunk is defined by an array of CHUNK_GRADIENT_ARRAY_SIZE * CHUNK_GRADIENT_ARRAY_SIZE of Gradient vectors  
	UPROPERTY(EditDefaultsOnly, meta=(ClampMin="0"))
	int ChunkGranularity = 16;
	
	// Multiplier for Perlin Noise output (default Perlin output is between -1 and 1; in practice usually between -0.5 and 0.5)
	// Default will be multiplied by this coefficient and further by 100 (to convert to centimeters for Unreal)
	UPROPERTY(EditDefaultsOnly, meta=(ClampMin="0"))
	int PerlinHeightMultiplier = 16;

	// From what height above 0 Coordinate snow will be generated
	UPROPERTY(EditDefaultsOnly)
	int SnowHeight = 8;

	// From what depth from surface a block will be replaced by a harder variant
	UPROPERTY(EditDefaultsOnly, meta=(ClampMin="0"))
	int HardenDepth = 8;

	// To what depth from surface blocks will be generated
	// Ideally down to -PerlinHeightMultiplier, but for quicker world generation this is customizable
	UPROPERTY(EditDefaultsOnly, meta=(ClampMin="0"))
	int GenerationDepth = 16;

	UPROPERTY()
	AWorldExtendDetector* WorldExtendDetector;
	
	// Class handling World generation
	UPROPERTY()
	UWorldGenerator* WorldGenerator;

};



