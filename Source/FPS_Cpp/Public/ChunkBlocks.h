// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "ChunkBlocks.generated.h"

/**
 * 
 */

USTRUCT()
struct FPS_CPP_API FUserBlockData
{
	GENERATED_BODY()

	FUserBlockData();
	FUserBlockData(FIntVector InPosition, TSubclassOf<ABlock> InBlockType);
	~FUserBlockData();

	UPROPERTY()
	FIntVector Position;
	
	UPROPERTY()
	TSubclassOf<ABlock> BlockType;
};

USTRUCT()
struct FPS_CPP_API FChunkBlocks
{
	GENERATED_BODY()

public:
	FChunkBlocks();
	FChunkBlocks(int InX, int InY);
	~FChunkBlocks();

	UPROPERTY()
	int X;
	
	UPROPERTY()
	int Y;
	
	UPROPERTY()
	TArray<ABlock*> WorldBlocks;
	
	UPROPERTY()
	TArray<ABlock*> UserBlocks;
	
	UPROPERTY()
	TArray<FIntVector> Mined;

	UPROPERTY()
	TArray<FUserBlockData> UserData;
};
