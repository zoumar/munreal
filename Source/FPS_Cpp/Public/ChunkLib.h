// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "Chunk.h"

class UWorldGenerator;

DECLARE_LOG_CATEGORY_EXTERN(LogChunkLib, Log, All);

/**
 *	Basic functions for structure holding gradients of a single game chunk
 *	If terrain is not modified, it is enough to save this data to recreate the world
 */

class FPS_CPP_API FChunkLib
{
public:
	enum EInterpolationMode
	{
		EIM_Linear,
		EIM_Cubic,
		EIM_Quintic,
	};
	
	// Fill in the FChunk.Gradients with random vectors with values between -1 and 1
	static void ComputeGradients(FChunk& Chunk);
	static void ComputeGradients(FChunk* Chunk);

	static void ComputeGradientSmoothingData(UWorldGenerator& WorldGenerator, FChunk& Chunk);
	static void ComputeGradientSmoothingData(UWorldGenerator& WorldGenerator, FChunk* Chunk);
	
	// Returns a copy of a vector with floored values
	static FVector FloorVector(FVector Vector);

	// Returns a copy of a vector with rounded values
	static FVector RoundVector(FVector Vector);

	// Returns a copy of an integer vector with rounded values
	static FIntVector RoundVectorToInt(const FVector& Vector);

	// Clamps the result to Start or End parameter if Weight parameter is outside of 0-1 range
	// Returns true if the Result was clamped, false otherwise when result was not changed
	static bool Clamp(const float& Start, const float& End, const float& Weight, float& Result);

	// Returns Linearly interpolated value  
	static float InterpolateLinear(float Start, float End, float Weight, bool bClamp);
	
	// Returns Cubically interpolated value
	static float InterpolateCubic(float Start, float End, float Weight, bool bClamp = true);
	
	// Returns Quintically interpolated value
	static float InterpolateQuintic(float Start, float End, float Weight, bool bClamp = true);

	// Returns the Dot product of the distance and gradient vectors 
	static float DotProductOfGridGradient(int GridX, int GridY, float X, float Y, const FChunk& Chunk);

	// Returns value of Perlin noise for supplied coordinates in supplied chunk
	// Possible to change interpolation mode
	static float PerlinNoise(const float X, const float Y, const FChunk& Chunk, EInterpolationMode InterpolationMode = EIM_Linear);

	// Compute chunk start coordinates from world position
	static FIntVector2 GetChunkCoordsFromPosition(const FVector& Position, int ChunkOffsetCm);
	
	// Return pointer to chunk with supplied start coordinates
	static FChunk* FindChunk(TArray<FChunk>& Chunks, const FIntVector2& StartCoordinates);
	static FChunk* FindChunk(TArray<FChunk>& Chunks, const int StartCoordinateX, const int StartCoordinateY);

	// Return index of chunk with supplied start coordinates
	static int FindChunkIndex(TArray<FChunk>& Chunks, const FIntVector2& StartCoordinates);
	static int FindChunkIndex(TArray<FChunk>& Chunks, const int StartCoordinateX, const int StartCoordinateY);
	
	// For Debug only - spawn Blocks into the world at 0,0,0 coordinates just to make sure Chunk.Gradients are OK 
	static void DebugChunkGradient(UWorld* World, FChunk* Chunk, TSubclassOf<ABlock> BlockType);

	// Log the Gradients array 
	static void LogChunkGradients(FChunk& Chunk);

	static void LogChunksStartCoords(TArray<FChunk>& Chunks);
};
