// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Chunk.generated.h"

// Define constants
#define CHUNK_GRADIENT_ARRAY_SIZE 4

// Define Macros

/**
 *	Basic structure holding gradients of a single game chunk and later
 *	computed position vectors of the surface block in the chunk
 *	If terrain is not modified, it is enough to save the Gradient data to recreate the world
 *	Now we save computing time rather than memory, so we save the position vectors of the surface block as well
 *
 *	Methods for FChunk are located in FChunkLib so that this class is as lightweight in memory as possibly
 */

// The inner array is enclosed in a struct to allow Unreal to use default serialization
USTRUCT()
struct  FPS_CPP_API FVectorArray
{
	GENERATED_BODY()
	
	UPROPERTY()
	TArray<FVector> Array;
};

// The inner array is enclosed in a struct to allow Unreal to use default serialization
USTRUCT()
struct  FPS_CPP_API FVector2fArray
{
	GENERATED_BODY()
	
	UPROPERTY()
	TArray<FVector2f> Array;
};

USTRUCT()
struct FPS_CPP_API FChunk
{
	GENERATED_BODY()

	FChunk();
	// Use the following constructor to allocate all needed memory and in turn allow parallelization
	FChunk(int InX, int InY, int Granularity);
	~FChunk();
	
	// Coordinates of starting corner of the chunk
	UPROPERTY()
	int X;
	
	UPROPERTY()
	int Y;

	// 2D Array of gradients used to generate the world
	// Size increased by 1 to enable smooth transition of neighboring chunks
	// The inner array is enclosed in a struct to allow Unreal to use default serialization
	UPROPERTY()
	TArray<FVector2fArray> Gradients;

	// Position Vectors of the surface block to save computation time later
	// The inner array is enclosed in a struct to allow Unreal to use default serialization
	UPROPERTY()
	TArray<FVectorArray> BlocksTopCoordinates;
};
