// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Chunk.h"
#include "ChunkBlocks.h"
#include "GameFramework/SaveGame.h"
#include "SavedWorld.generated.h"

/**
 * 
 */
UCLASS()
class FPS_CPP_API USavedWorld : public USaveGame//ULocalPlayerSaveGame
{
	GENERATED_BODY()

public:
	USavedWorld();
	USavedWorld(int BlockTypeCount, int ChunkGranularity, int PerlinHeightMultiplier, int SnowHeight, int HardenDepth,
	            FTransform PlayerTransform, TArray<FChunk> Chunks, TArray<FChunkBlocks> ChunkBlocks);
	
	// How many block types are used
	UPROPERTY()
	int BlockTypeCount;

	// How many blocks will be generated between each gradient vector in a chunk
	// Chunk is defined by an array of CHUNK_GRADIENT_ARRAY_SIZE * CHUNK_GRADIENT_ARRAY_SIZE of Gradient vectors
	UPROPERTY()
	int ChunkGranularity;
	
	// Multiplier for Perlin Noise output (default Perlin output is between -1 and 1; in practice usually between -0.5 and 0.5)
	// Default will be multiplied by this coefficient and further by 100 (to convert to centimeters for Unreal)
	UPROPERTY()
	int PerlinHeightMultiplier;

	// From what height above 0 Coordinate snow will be generated
	UPROPERTY()
	int SnowHeight;

	// From what depth from surface a block will be replaced by a harder variant
	UPROPERTY()
	int HardenDepth;

	// Player transform
	UPROPERTY()
	FTransform PlayerTransform;

	// Data of generated chunks
	UPROPERTY()
	TArray<FChunk> Chunks;

	// Data of Block actors and mined blocks
	UPROPERTY()
	TArray<FChunkBlocks> ChunkBlocks;
};
