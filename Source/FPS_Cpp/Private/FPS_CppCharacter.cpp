// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPS_CppCharacter.h"

#include "WorldGenerator.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "FPS_CppGameMode.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// AFPS_CppCharacter

AFPS_CppCharacter::AFPS_CppCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(45.f, 96.0f);
	
	WorldExtendCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("WorldExtendDetector"));
	WorldExtendCollider->SetupAttachment(RootComponent);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->SetupAttachment(RootComponent);

	// Create a mesh component for displaying where the player can place blocks
	MeshAimBox = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BoxAimMesh"));
	MeshAimBox->SetOnlyOwnerSee(true);
	MeshAimBox->bCastDynamicShadow = false;
	MeshAimBox->CastShadow = false;
	MeshAimBox->SetVisibility(false);
	MeshAimBox->SetupAttachment(RootComponent);
}

void AFPS_CppCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	// Add Input Mapping Context
	if (const APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	WorldGenerator = Cast<AFPS_CppGameMode>(GetWorld()->GetAuthGameMode())->WorldGenerator;

	SetupWorldExtendDetectors();
}

void AFPS_CppCharacter::SetupWorldExtendDetectors()
{
	double Scale = CHUNK_GRADIENT_ARRAY_SIZE * Cast<AFPS_CppGameMode>(GetWorld()->GetAuthGameMode())->ChunkGranularity;
	WorldExtendCollider->SetWorldScale3D(FVector(Scale, Scale, Scale));
}

#pragma region Input
//////////////////////////////////////////////////////////////////////////// Input

void AFPS_CppCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AFPS_CppCharacter::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AFPS_CppCharacter::Look);

		// Mining
		EnhancedInputComponent->BindAction(MineAction, ETriggerEvent::Triggered, this, &AFPS_CppCharacter::Mine);
		EnhancedInputComponent->BindAction(MineAction, ETriggerEvent::Completed, this, &AFPS_CppCharacter::StopMining);

		// Placing
		EnhancedInputComponent->BindAction(PlaceAction, ETriggerEvent::Started, this, &AFPS_CppCharacter::PlaceBlock);
		
		// Selecting block type to place
		EnhancedInputComponent->BindAction(NextBlockTypeAction, ETriggerEvent::Started, this, &AFPS_CppCharacter::NextBlockType);
		EnhancedInputComponent->BindAction(PreviousBlockTypeAction, ETriggerEvent::Started, this, &AFPS_CppCharacter::PreviousBlockType);
		
		// Saving and loading world
		EnhancedInputComponent->BindAction(SaveWorldAction, ETriggerEvent::Started, this, &AFPS_CppCharacter::SaveWorld);
		EnhancedInputComponent->BindAction(LoadWorldAction, ETriggerEvent::Started, this, &AFPS_CppCharacter::LoadWorld);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error,
		       TEXT(
			       "'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input "
			       "system. If you intend to use the legacy system, then you will need to update this C++ file."
		       ), *GetNameSafe(this));
	}
}

void AFPS_CppCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	const FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void AFPS_CppCharacter::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void AFPS_CppCharacter::ResetMiningStatus()
{
	bIsMining = false;
	MinedBlock = nullptr;
	TimeToMine = 0;
}

void AFPS_CppCharacter::Mine(const FInputActionValue& Value)
{
	if (LookedAtBlock == nullptr)
	{
		ResetMiningStatus();
		return;
	}
	if (MinedBlock != LookedAtBlock)
	{
		// Start mining new block
		bIsMining = true;
		MinedBlock = LookedAtBlock;
		TimeToMine = MinedBlock->TimeToMine;
	}
	else if(TimeToMine <= 0)
	{
		// Finished mining the block
		// TODO Refactor through Events? Would there be safe order of execution?
		WorldGenerator->MineBlockFromChunk(MinedBlock);
		MinedBlock->Destroy();
		ResetMiningStatus();
	}
}

void AFPS_CppCharacter::StopMining(const FInputActionValue& Value)
{
	ResetMiningStatus();
}

void AFPS_CppCharacter::PlaceBlock(const FInputActionValue& Value)
{
	// Check if user can place block
	if (bIsMining || !bIsAimBoxPositionValid)
	{	
		return;
	}

	// Get GameMode and from it defined block types
	AFPS_CppGameMode* GameMode = static_cast<AFPS_CppGameMode*>(GetWorld()->GetAuthGameMode());
	const TSubclassOf<ABlock> BlockType = GameMode->BlockTypes[SelectedBlockType];

	// Spawn new block at desired location
	// TODO Perform collision check; The following always reports collision, so other solution is required, maybe UWorld::OverlapMultiByChannel()? 
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	// ABlock* NewBlock = GetWorld()->SpawnActor<ABlock>(AimBoxPosition, FRotator(), SpawnParams);
	ABlock* NewBlock = GetWorld()->SpawnActor<ABlock>(BlockType, AimBoxPosition, FRotator(0,0,0));
	WorldGenerator->AddBlockToChunk(NewBlock);
	// ABlock* NewBlock = GetWorld()->SpawnActor<ABlock>(BlockType, AimBoxPosition, FRotator());
	// if (!NewBlock)
	// {
	// 	DebugScreenLog(TEXT("Unable to spawn block - colliding"));
	// 	return;
	// }
}

void AFPS_CppCharacter::NextBlockType(const FInputActionValue& Value)
{
	SelectedBlockType = SelectedBlockType + 1;  
	SelectedBlockType %= static_cast<AFPS_CppGameMode*>(GetWorld()->GetAuthGameMode())->BlockTypes.Num();
}

void AFPS_CppCharacter::PreviousBlockType(const FInputActionValue& Value)
{
	if(--SelectedBlockType < 0)
	{
		SelectedBlockType = static_cast<AFPS_CppGameMode*>(GetWorld()->GetAuthGameMode())->BlockTypes.Num() -1;
	}
}

void AFPS_CppCharacter::SaveWorld(const FInputActionValue& Value)
{
	DebugScreenLog(TEXT("Saving World"));

	WorldGenerator->SaveWorld(*this);
}

void AFPS_CppCharacter::LoadWorld(const FInputActionValue& Value)
{
	DebugScreenLog(TEXT("Loading World"));

	WorldGenerator->LoadWorld(*this);
}

#pragma endregion Input

void AFPS_CppCharacter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Debug Box of World Extend Collider
	// DrawDebugBox(GetWorld(), WorldExtendCollider->GetComponentLocation(), WorldExtendCollider->GetScaledBoxExtent(),
	// 	FColor::Red, false, 0.05, 0, 5);

	UpdateAimBoxProperties();
	UpdateMiningStatus(DeltaSeconds);
}

void AFPS_CppCharacter::UpdateAimBoxProperties(const bool bDisplayAimBox)
{
	const FHitResult Hit = RayCastForBuilding();
	if (!Hit.IsValidBlockingHit())
	{
		// Nothing hit, set variables and quit
		LookedAtBlock = nullptr;
		MeshAimBox->SetVisibility(false);
		bIsAimBoxPositionValid = false;
		return;
	}
	bIsAimBoxPositionValid = true;
	// Save the block we are looking at
	LookedAtBlock = Cast<ABlock>(Hit.GetActor());

	if(bIsMining && LookedAtBlock)
	{
		// When mining, signify it by overlapping the AimBox over mined block
		AimBoxPosition = LookedAtBlock->GetActorLocation();
	}
	else
	{
		// Get the aimBox position
		AimBoxPosition = Hit.ImpactPoint + Hit.ImpactNormal / 2;
		// Align final aimBox position to world grid
		AimBoxPosition = FChunkLib::FloorVector(AimBoxPosition / 100) * 100;
	}
	
	if (bDisplayAimBox)
	{
		MeshAimBox->SetVisibility(true);
		MeshAimBox->SetWorldTransform(FTransform(FRotator(0,0,0), AimBoxPosition));
	}
}

void AFPS_CppCharacter::UpdateMiningStatus(const float DeltaSeconds)
{
	if (bIsMining)
	{
		TimeToMine -= DeltaSeconds;
	}
}

FHitResult AFPS_CppCharacter::RayCastForBuilding(const bool bDrawDebugLine) const
{	
	// Get the camera transform
	FVector CameraLocation;
	FRotator CameraRotation;
	Controller->GetPlayerViewPoint(CameraLocation, CameraRotation);

	// Calculate end position for rayCast
	const FVector EndLocation = CameraLocation + CameraRotation.Vector() * MaxInteractDistance;

	// Perform the rayCast
	FHitResult Hit;
	GetWorld()->LineTraceSingleByChannel(Hit, CameraLocation, EndLocation, ECC_Visibility);

	if (bDrawDebugLine)
	{
		// Debug draw the ray
		DrawDebugLine(GetWorld(), CameraLocation, EndLocation, FColor::Red, false, .05f, 0, 1.0f);		
	}
	return Hit;
}

void AFPS_CppCharacter::DebugScreenLog(const FString& Message)
{	
	GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, Message);	
}