// Fill out your copyright notice in the Description page of Project Settings.

#include "ChunkLib.h"

#include "WorldGenerator.h"

DEFINE_LOG_CATEGORY(LogChunkLib);

// Gradient functions ///////////////////////////////////////////////////////////////////////////

void FChunkLib::ComputeGradients(FChunk* Chunk)
{
	ComputeGradients(*Chunk);
}

void FChunkLib::ComputeGradients(FChunk& Chunk)
{
	for (int X = 0; X < CHUNK_GRADIENT_ARRAY_SIZE /*+ 1*/; X++)
	{
		for (int Y = 0; Y < CHUNK_GRADIENT_ARRAY_SIZE /*+ 1*/; Y++)
		{
			// TODO Optimize performance by getting rid of FRand and using some bit shifts and such
			Chunk.Gradients[X].Array[Y] = StaticCast<FVector2f>(FMath::RandPointInCircle(1));
			// UE_LOG(LogChunkLib, Log, TEXT("	Gradient[%d][%d] is (%f %f)"), X, Y, Chunk.Gradients[X][Y].X, Chunk.Gradients[X][Y].Y);
		}
	}
	// Dont leave random data in the last row and column
	for (int X = 0; X < CHUNK_GRADIENT_ARRAY_SIZE; X++)
	{
		Chunk.Gradients[X].Array[CHUNK_GRADIENT_ARRAY_SIZE] = Chunk.Gradients[X].Array[CHUNK_GRADIENT_ARRAY_SIZE - 1];
	}
	for (int Y = 0; Y < CHUNK_GRADIENT_ARRAY_SIZE + 1; Y++)
	{
		Chunk.Gradients[CHUNK_GRADIENT_ARRAY_SIZE].Array[Y] = Chunk.Gradients[CHUNK_GRADIENT_ARRAY_SIZE -1 ].Array[Y];
	}
}

void FChunkLib::ComputeGradientSmoothingData(UWorldGenerator& WorldGenerator, FChunk* Chunk)
{
	ComputeGradientSmoothingData(WorldGenerator, *Chunk);
}

void FChunkLib::ComputeGradientSmoothingData(UWorldGenerator& WorldGenerator, FChunk& Chunk)
{
	UE_LOG(LogChunkLib, Log, TEXT("    Smooth chunk %d %d; offset %d"), Chunk.X, Chunk.Y,
		WorldGenerator.GetChunkOffsetCm());

	
	// Fill X axis
	int WantedStartX = Chunk.X + WorldGenerator.GetChunkOffsetCm();
	int WantedStartY = Chunk.Y;

	FChunk* OtherChunk = FindChunk(WorldGenerator.GetChunks(), WantedStartX, WantedStartY);
	if (OtherChunk == nullptr)
	{
		UE_LOG(LogChunkLib, Warning, TEXT("No neighboring chunk in X axis with coordinates %d %d found during FillGradientSmootingData"),
			WantedStartX, WantedStartY);
	}
	else
	{
		for (int X = 0; X < CHUNK_GRADIENT_ARRAY_SIZE; X++)
		{
			Chunk.Gradients[X].Array[CHUNK_GRADIENT_ARRAY_SIZE] = OtherChunk->Gradients[X].Array[0];
		}
	}

	// Fill Y axis
	WantedStartX = Chunk.X;
	WantedStartY = Chunk.Y + WorldGenerator.GetChunkOffsetCm();

	OtherChunk = FindChunk(WorldGenerator.GetChunks(), WantedStartX, WantedStartY);
	if (OtherChunk == nullptr)
	{
		UE_LOG(LogChunkLib, Warning, TEXT("No neighboring chunk in Y axis with coordinates %d %d found during FillGradientSmootingData"),
			WantedStartX, WantedStartY);
	}
	else
	{
		for (int Y = 0; Y < CHUNK_GRADIENT_ARRAY_SIZE; Y++)
		{
			Chunk.Gradients[CHUNK_GRADIENT_ARRAY_SIZE].Array[Y] = OtherChunk->Gradients[0].Array[Y];
		}
	}

	// Fill the corner
	WantedStartX = Chunk.X + WorldGenerator.GetChunkOffsetCm();
	WantedStartY = Chunk.Y + WorldGenerator.GetChunkOffsetCm();

	OtherChunk = FindChunk(WorldGenerator.GetChunks(), WantedStartX, WantedStartY);
	if (OtherChunk == nullptr)
	{
		UE_LOG(LogChunkLib, Warning, TEXT("No neighboring chunk in corner with coordinates %d %d found during FillGradientSmootingData"),
			WantedStartX, WantedStartY);
	}
	else
	{
		Chunk.Gradients[CHUNK_GRADIENT_ARRAY_SIZE].Array[CHUNK_GRADIENT_ARRAY_SIZE] = OtherChunk->Gradients[0].Array[0];
	}
}

// Perlin noise functions ///////////////////////////////////////////////////////////////////////

bool FChunkLib::Clamp(const float & Start,const float& End, const float& Weight, float& Result)
{	
	if (Weight < 0)
	{
		Result = Start;
		return true;		
	}
	if (Weight > 1)
	{
		Result = End;
		return true;
	}
	return false;
}

float FChunkLib::InterpolateLinear(const float Start, const float End, const float Weight, const bool bClamp)
{
	float Result;
	if (bClamp && Clamp(Start, End, Weight, Result))
	{
		return Result;
	}
	Result = (End - Start) * Weight + Start;
	return Result;
}

float FChunkLib::InterpolateCubic(const float Start, const float End, const float Weight, const bool bClamp)
{
	float Result;
	if (bClamp && Clamp(Start, End, Weight, Result))
	{
		return Result;
	}
	Result = (End - Start) * (3.0 - Weight * 2.0) * Weight * Weight + Start;
	return Result;
}

float FChunkLib::InterpolateQuintic(const float Start, const float End, const float Weight, const bool bClamp)
{
	float Result;
	if (bClamp && Clamp(Start, End, Weight, Result))
	{
		return Result;
	}
	Result = (End - Start) * ((Weight * (Weight * 6.0 - 15.0) + 10.0) * Weight * Weight * Weight) + Start;
	return Result;
}

float FChunkLib::DotProductOfGridGradient(const int GridX,const  int GridY, const float X, const float Y, const FChunk& Chunk) {
	// Compute the distance vector from Grid point
	const float DX = X - GridX;
	const float DY = Y - GridY;

	// Compute the dot-product of distance vector and gradient vector
	return (DX * Chunk.Gradients[GridX].Array[GridY].X + DY * Chunk.Gradients[GridX].Array[GridY].Y);
}

float FChunkLib::PerlinNoise(const float X, const float Y, const FChunk& Chunk, const EInterpolationMode InterpolationMode)
{
	// UE_LOG(LogChunkLib, Log, TEXT("Perlin inputs: X:%f; Y:%f"), X, Y);
	// Get grid corners for supplied coordinates
	const int X0 = floor(X);
	const int Y0 = floor(Y);
	// These will not overflow since we have bigger array for smooth transitions between neighboring chunks
	const int X1 = X0 + 1;
	const int Y1 = Y0 + 1;
	// const int X1 = FMath::Min(X0 + 1, CHUNK_GRADIENT_ARRAY_SIZE - 1);
	// const int Y1 = FMath::Min(Y0 + 1, CHUNK_GRADIENT_ARRAY_SIZE - 1);

	// UE_LOG(LogChunkLib, Log, TEXT("X0:%d; Y0:%d; X1:%d; Y1:%d"), X0, Y0, X1, Y1);
	
	// Compute weights for interpolation
	const float WeightX = X - X0;
	const float WeightY = Y - Y0;
	// UE_LOG(LogChunkLib, Log, TEXT("WeightX:%f; \nWeightY:%f"), WeightX, WeightY);

	// Compute Dot products 
	const float DotProductX0Y0 = DotProductOfGridGradient(X0, Y0, X, Y, Chunk);
	const float DotProductX1Y0 = DotProductOfGridGradient(X1, Y0, X, Y, Chunk);
	const float DotProductX0Y1 = DotProductOfGridGradient(X0, Y1, X, Y, Chunk);
	const float DotProductX1Y1 = DotProductOfGridGradient(X1, Y1, X, Y, Chunk);
	// UE_LOG(LogChunkLib, Log, TEXT("DotProductX0Y0:%f; \nDotProductX1Y0:%f; \nDotProductX0Y1:%f; \nDotProductX1Y1:%f"),
	// 	DotProductX0Y0, DotProductX1Y0, DotProductX0Y1, DotProductX1Y1);

	float (*Interpolate) (float, float, float, bool);
	switch (InterpolationMode)
	{
	case EIM_Quintic:
		Interpolate = InterpolateQuintic; 
		break;
	case EIM_Cubic:
		Interpolate = InterpolateCubic;
		break;
	case EIM_Linear:
	default:
		Interpolate = InterpolateLinear;
		break;
	}
	const float InterpolatedHorizontalY0 = Interpolate(DotProductX0Y0, DotProductX1Y0, WeightX, true);
	const float InterpolatedHorizontalY1 = Interpolate(DotProductX0Y1, DotProductX1Y1, WeightX, true);
	const float InterpolatedVertical = Interpolate(InterpolatedHorizontalY0, InterpolatedHorizontalY1, WeightY, true);

	// UE_LOG(LogChunkLib, Log, TEXT(" InterpolatedHorizontalY0: %f"), InterpolatedHorizontalY0);
	// UE_LOG(LogChunkLib, Log, TEXT(" InterpolatedHorizontalY1: %f"), InterpolatedHorizontalY1);
	// UE_LOG(LogChunkLib, Log, TEXT(" Perlin output: %f"), InterpolatedVertical);
	return InterpolatedVertical;
}

// Other functions /////////////////////////////////////////////////////////////////////////////

FVector FChunkLib::FloorVector(FVector Vector)
{
	Vector.X = floor(Vector.X);
	Vector.Y = floor(Vector.Y);
	Vector.Z = floor(Vector.Z);
	return Vector;
}

FVector FChunkLib::RoundVector(FVector Vector)
{
	Vector.X = round(Vector.X);
	Vector.Y = round(Vector.Y);
	Vector.Z = round(Vector.Z);
	return Vector;
}

FIntVector FChunkLib::RoundVectorToInt(const FVector& Vector)
{
	return FIntVector(
		round(Vector.X),
		round(Vector.Y),
		round(Vector.Z)
	);
}

FIntVector2 FChunkLib::GetChunkCoordsFromPosition(const FVector& Position, int ChunkOffsetCm)
{
	const FIntVector IntPosition = RoundVectorToInt(Position);
	// Integer divide
	FIntVector2 Result = FIntVector2(IntPosition.X / ChunkOffsetCm * ChunkOffsetCm,
	                                 IntPosition.Y / ChunkOffsetCm * ChunkOffsetCm);
	// But for negative numbers subtract one more chunk
	if (IntPosition.X < 0)
	{
		Result.X -= ChunkOffsetCm;
	}
	if (IntPosition.Y < 0)
	{
		Result.Y -= ChunkOffsetCm;
	}	
	return Result;
}

FChunk* FChunkLib::FindChunk(TArray<FChunk>& Chunks, const FIntVector2& StartCoordinates)
{
	return FindChunk(Chunks, StartCoordinates.X, StartCoordinates.Y);
}

FChunk* FChunkLib::FindChunk(TArray<FChunk>& Chunks, const int StartCoordinateX, const int StartCoordinateY)
{
	return Chunks.FindByPredicate([StartCoordinateX, StartCoordinateY](FChunk& Test)
	{
		return Test.X == StartCoordinateX && Test.Y == StartCoordinateY;
	});
}

int FChunkLib::FindChunkIndex(TArray<FChunk>& Chunks, const FIntVector2& StartCoordinates)
{
	return FindChunkIndex(Chunks, StartCoordinates.X, StartCoordinates.Y);
}

int FChunkLib::FindChunkIndex(TArray<FChunk>& Chunks, const int StartCoordinateX, const int StartCoordinateY)
{
	int Index = 0;
	for (; Index < Chunks.Num(); Index++)
	{
		if (Chunks[Index].X == StartCoordinateX &&
			Chunks[Index].Y == StartCoordinateY)
		{
			break;
		}
	}
	return Index;
}

void FChunkLib::DebugChunkGradient(UWorld* World, FChunk* Chunk, TSubclassOf<ABlock> BlockType)
{
	const FRotator ZR = FRotator(0,0,0); // Zero rotator
	for(int x = 0; x < CHUNK_GRADIENT_ARRAY_SIZE; x++)
	{
		for(int y = 0; y < CHUNK_GRADIENT_ARRAY_SIZE; y++)
		{
			FVector Pos = FVector(x * 100, y * 100, Chunk->Gradients[x].Array[y][0] * 1000);
			World->SpawnActor<ABlock>(BlockType, Pos, ZR);
			Pos = FVector(CHUNK_GRADIENT_ARRAY_SIZE*100 + x*100, y*100, Chunk->Gradients[x].Array[y][1]*1000);
			World->SpawnActor<ABlock>(BlockType, Pos, ZR);
		}
	}
}

void FChunkLib::LogChunkGradients(FChunk& Chunk)
{
	for (int GradientX = 0; GradientX < CHUNK_GRADIENT_ARRAY_SIZE; GradientX++)
	{
		for (int GradientY = 0; GradientY < CHUNK_GRADIENT_ARRAY_SIZE; GradientY++)
		{
			UE_LOG(LogChunkLib, Log, TEXT("Grad %d %d : %f %f"), GradientX, GradientY,
				Chunk.Gradients[GradientX].Array[GradientY].X, Chunk.Gradients[GradientX].Array[GradientY].Y);
		}
	}
}

void FChunkLib::LogChunksStartCoords(TArray<FChunk>& Chunks)
{
	for (FChunk& Chunk : Chunks)
	{
		UE_LOG(LogChunkLib, Log, TEXT("  Chunk start coords %d %d"), Chunk.X, Chunk.Y);
	}
}
