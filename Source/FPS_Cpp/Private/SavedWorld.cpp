// Fill out your copyright notice in the Description page of Project Settings.


#include "SavedWorld.h"

USavedWorld::USavedWorld(): BlockTypeCount(0), ChunkGranularity(0), PerlinHeightMultiplier(0), SnowHeight(0),
                            HardenDepth(0)
{
}

USavedWorld::USavedWorld(
	int BlockTypeCount, int ChunkGranularity, int PerlinHeightMultiplier, int SnowHeight, int HardenDepth,
	FTransform PlayerTransform, TArray<FChunk> Chunks, TArray<FChunkBlocks> ChunkBlocks)
{
	this->BlockTypeCount = BlockTypeCount;
	this->ChunkGranularity = BlockTypeCount;
	this->PerlinHeightMultiplier = PerlinHeightMultiplier;
	this->SnowHeight = SnowHeight;
	this->HardenDepth = HardenDepth;
	this->PlayerTransform = PlayerTransform;
	this->Chunks = Chunks;
	this->ChunkBlocks = ChunkBlocks;
}
