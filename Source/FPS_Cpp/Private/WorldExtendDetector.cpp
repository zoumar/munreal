// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldExtendDetector.h"

#include "FPS_CppCharacter.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(LogWorldExtendDetector);

// Sets default values
AWorldExtendDetector::AWorldExtendDetector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<UBoxComponent>(TEXT("Root"));
	
	YPNorth = CreateDefaultSubobject<UBoxComponent>(TEXT("YPNorthCollision"));
	YNSouth = CreateDefaultSubobject<UBoxComponent>(TEXT("YNSouthCollision"));
	XPWest = CreateDefaultSubobject<UBoxComponent>(TEXT("XPEastCollision"));
	XNEast = CreateDefaultSubobject<UBoxComponent>(TEXT("XNWestCollision"));

	SetRootComponent(Root);
	
	YPNorth->SetupAttachment(RootComponent);
	YNSouth->SetupAttachment(RootComponent);
	XPWest->SetupAttachment(RootComponent);
	XNEast->SetupAttachment(RootComponent);

	YPNorth->OnComponentBeginOverlap.AddDynamic(this, &AWorldExtendDetector::OnBoxOverlapBegin);
	YNSouth->OnComponentBeginOverlap.AddDynamic(this, &AWorldExtendDetector::OnBoxOverlapBegin);
	XPWest->OnComponentBeginOverlap.AddDynamic(this, &AWorldExtendDetector::OnBoxOverlapBegin);
	XNEast->OnComponentBeginOverlap.AddDynamic(this, &AWorldExtendDetector::OnBoxOverlapBegin);
}

// Called when the game starts or when spawned
void AWorldExtendDetector::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWorldExtendDetector::InitMembers(AFPS_CppGameMode& GameMode)
{
	// TODO Tune the values
	ChunkGranularity = GameMode.ChunkGranularity;
	WorldGenerator = GameMode.WorldGenerator;
	// ColliderHeight = 9 * GameMode.PerlinHeightMultiplier;
	// BoundaryDistance = CHUNK_GRADIENT_ARRAY_SIZE * ChunkGranularity;
	BoundaryDistance = CHUNK_GRADIENT_ARRAY_SIZE * ChunkGranularity * 100;
}

void AWorldExtendDetector::SetupBoundaries()
{
	
	YPNorth->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(0,BoundaryDistance,0),
		UE::Math::TVector<double>(BoundaryDistance,1, BoundaryDistance)));
	YNSouth->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(0,-BoundaryDistance,0),
		UE::Math::TVector<double>(BoundaryDistance, 1, BoundaryDistance)));
	XPWest->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(BoundaryDistance,0,0),
		UE::Math::TVector<double>(1,BoundaryDistance, BoundaryDistance)));
	XNEast->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(-BoundaryDistance,0,0),
		UE::Math::TVector<double>(1,BoundaryDistance, BoundaryDistance)));

	/*
	YPNorth->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(0,BoundaryDistance,0),
		UE::Math::TVector<double>(9 * BoundaryDistance,1, ColliderHeight)));
	YNSouth->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(0,-BoundaryDistance,0),
		UE::Math::TVector<double>(9 * BoundaryDistance, 1, ColliderHeight)));
	XPWest->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(BoundaryDistance,0,0),
		UE::Math::TVector<double>(1,9 * BoundaryDistance, ColliderHeight)));
	XNEast->SetWorldTransform(FTransform(
		UE::Math::TRotator<double>(),
		UE::Math::TVector<double>(-BoundaryDistance,0,0),
		UE::Math::TVector<double>(1,9 * BoundaryDistance, ColliderHeight)));
	*/
	
}

// Called every frame
void AWorldExtendDetector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*
	// Debug boxes - with giant scale badly visible - for debug change in SetupBoundaries
	DrawDebugBox(GetWorld(), YPNorth->GetComponentLocation(), YPNorth->GetScaledBoxExtent(),
		FColor::Blue, false, 0.05, 0, 5);
	DrawDebugBox(GetWorld(), YNSouth->GetComponentLocation(), YNSouth->GetScaledBoxExtent(),
		FColor::Cyan, false, 0.05, 0, 5);
	DrawDebugBox(GetWorld(), XPWest->GetComponentLocation(), XPWest->GetScaledBoxExtent(),
		FColor::Emerald, false, 0.05, 0, 5);
	DrawDebugBox(GetWorld(), XNEast->GetComponentLocation(), XNEast->GetScaledBoxExtent(),
		FColor::Green, false, 0.05, 0, 5);
	*/	
}

void AWorldExtendDetector::OnBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// UE_LOG(LogWorldExtendDetector, Log, TEXT("Begin Overlap of %s with %s of %s"),*OverlappedComponent->GetName(), *OtherComp->GetName(), *OtherActor->GetName());

	if (!Cast<AFPS_CppCharacter>(OtherActor) || !Cast<UBoxComponent>(OtherComp))
	{
		return;
	}
	UE_LOG(LogWorldExtendDetector, Log, TEXT("Begin Overlap of %s with %s of %s"),
		*OverlappedComponent->GetName(), *OtherComp->GetName(), *OtherActor->GetName());
	
	FVector PlayerPosition = UGameplayStatics::GetPlayerController(GetWorld(),0)->GetPawn()->GetActorLocation();
	SetActorLocation(PlayerPosition);

	WorldGenerator->AdjustWorldToPlayerPosition(PlayerPosition);
}

