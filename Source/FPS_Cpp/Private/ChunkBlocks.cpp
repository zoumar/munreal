// Fill out your copyright notice in the Description page of Project Settings.


#include "..\Public\ChunkBlocks.h"

FUserBlockData::FUserBlockData(): Position()
{
}

FUserBlockData::FUserBlockData(FIntVector InPosition, TSubclassOf<ABlock> InBlockType)
{
	Position = InPosition;
	BlockType = InBlockType;
}

FUserBlockData::~FUserBlockData()
{
}

FChunkBlocks::FChunkBlocks(): X(0), Y(0)
{
}

FChunkBlocks::FChunkBlocks(int InX, int InY)
{
	X = InX;
	Y = InY;
}

FChunkBlocks::~FChunkBlocks()
{
}
