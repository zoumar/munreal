// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldGenerator.h"

#include "Block.h"
#include "FPS_CppCharacter.h"
#include "FPS_CppGameMode.h"
#include "SavedWorld.h"
#include "Kismet/GameplayStatics.h"
#include "Logging/LogMacros.h"

DEFINE_LOG_CATEGORY(LogWorldGen);

#define BLOCK_TYPE_COUNT 4

// TODO Reorganize for reader comfort - important methods at the start 

TArray<FChunk>& UWorldGenerator::GetChunks()
{
	return Chunks;
}

int UWorldGenerator::GetPerlinHeightMultiplier() const
{
	return PerlinHeightMultiplier;
}

double UWorldGenerator::GetPerlinHeightMultiplierCm() const
{
	return PerlinHeightMultiplierCm;
}

double UWorldGenerator::GetSnowHeightCm() const
{
	return SnowHeightCm;
}

double UWorldGenerator::GetHardenDepthCm() const
{
	return HardenDepthCm;
}

int UWorldGenerator::GetChunkGranularity() const
{
	return ChunkGranularity;
}

int UWorldGenerator::GetChunkOffset() const
{
	return ChunkOffset;
}

int UWorldGenerator::GetChunkOffsetCm() const
{
	return ChunkOffsetCm;
}

bool UWorldGenerator::ValidateInputsOfWorldGeneration() const
{
	if (!GetWorld())
	{
		UE_LOG(LogWorldGen, Error, TEXT("Invalid World supplied. Exiting"));
		return false;
	}
	const AFPS_CppGameMode* GameModeTemp = Cast<AFPS_CppGameMode>(GetWorld()->GetAuthGameMode());
	if (!GameModeTemp)
	{
		UE_LOG(LogWorldGen, Error, TEXT("Invalid Game Mode supplied. Exiting"));
		return false;
	}
	if (GameModeTemp->BlockTypes.Num() == 0)
	{
		UE_LOG(LogWorldGen, Error, TEXT("No Block types supplied. Exiting"));
		return false;
	}
	if (GameModeTemp->BlockTypes.Num() < BLOCK_TYPE_COUNT)
	{
		UE_LOG(LogWorldGen, Error, TEXT("Not enough Block types supplied for current implementation of world generation logic. Exiting"));
		return false;
	}
	return true;
}

void UWorldGenerator::InitMembers()
{
	GameMode = Cast<AFPS_CppGameMode>(GetWorld()->GetAuthGameMode());
	
	// Precompute centimeter values;
	PerlinHeightMultiplier = GameMode->PerlinHeightMultiplier;
	PerlinHeightMultiplierCm = GameMode->PerlinHeightMultiplier * 100;
	SnowHeightCm = GameMode->SnowHeight * 100;
	HardenDepthCm = GameMode->HardenDepth * 100;

	ChunkGranularity = GameMode->ChunkGranularity;

	// Precompute Chunk size
	ChunkOffset = CHUNK_GRADIENT_ARRAY_SIZE * ChunkGranularity;
	ChunkOffsetCm = ChunkOffset * 100;
}

UClass* UWorldGenerator::GetBlockClass(const ABlock* Block) const
{
	// Check against each child class
	for (UClass* ChildClass : GameMode->BlockTypes)
	{
		if (Block->IsA(ChildClass))
		{
			return ChildClass;
		}
	}
	return nullptr;
}

// Generate Block Heights ///////////////////////////////////////////////////////////////////

void UWorldGenerator::ComputeBlockHeights(UWorldGenerator& RWorldGenerator, int ChunkX, int ChunkY,
	FChunkLib::EInterpolationMode InterpolationMode)
{
	FChunk* Chunk = FChunkLib::FindChunk(RWorldGenerator.Chunks, ChunkX, ChunkY);
	if(!Chunk)
	{
		UE_LOG(LogWorldGen, Warning, TEXT("Not found chunk (%d %d)"), ChunkX, ChunkY);
		return;
	}
	ComputeBlockHeights(RWorldGenerator, *Chunk, InterpolationMode);
}

void UWorldGenerator::ComputeBlockHeights(UWorldGenerator& RWorldGenerator, int ChunkIndex,
	FChunkLib::EInterpolationMode InterpolationMode)
{
	FChunk& Chunk = RWorldGenerator.Chunks[ChunkIndex];
	ComputeBlockHeights(RWorldGenerator, Chunk, InterpolationMode);
}

void UWorldGenerator::ComputeBlockHeights(UWorldGenerator& RWorldGenerator, FChunk* Chunk,
	FChunkLib::EInterpolationMode InterpolationMode)
{
	ComputeBlockHeights(RWorldGenerator, *Chunk, InterpolationMode);
}

void UWorldGenerator::ComputeBlockHeights(UWorldGenerator& RWorldGenerator, FChunk& Chunk,
	FChunkLib::EInterpolationMode InterpolationMode)
{
	// TODO optimize the poop out of these loops; multiThread impossible since spawning Actors is possible only in Game Thread
	// Multiplication is faster than division, so we make the division ahead of time for incrementation for Perlin
	const double IncrementUnit = 1. / static_cast<double>(RWorldGenerator.ChunkGranularity);
	// Loop over all Gradients of the Chunk
	for (int GradientX = 0; GradientX < CHUNK_GRADIENT_ARRAY_SIZE; GradientX++)
	{
		for (int GradientY = 0; GradientY < CHUNK_GRADIENT_ARRAY_SIZE; GradientY++)
		{
			// For each Grid cell of Gradients generate sector of CHUNK_GRANULARITY * CHUNK_GRANULARITY blocks
			for (int GranularX = 0; GranularX < RWorldGenerator.ChunkGranularity; GranularX++)
			{
				const double X = GradientX + GranularX * IncrementUnit;
				// Multiply coordinates by 100, because 1 in Unreal is 1 cm
				FVector Position(Chunk.X + (GradientX * RWorldGenerator.ChunkGranularity + GranularX) * 100,0,0);
				
				for (int GranularY = 0; GranularY < RWorldGenerator.ChunkGranularity; GranularY++)
				{
					const double Y = GradientY + GranularY * IncrementUnit;
					
					Position.Y = Chunk.Y + (GradientY * RWorldGenerator.ChunkGranularity + GranularY) * 100;
					Position.Z = FChunkLib::PerlinNoise(X, Y, Chunk, InterpolationMode);
					Position.Z = floor(Position.Z * RWorldGenerator.PerlinHeightMultiplier) * 100;
					Chunk.BlocksTopCoordinates[GradientX * RWorldGenerator.ChunkGranularity + GranularX].Array[GradientY * RWorldGenerator.ChunkGranularity + GranularY] = Position;
					// UE_LOG(LogWorldGen, Log, TEXT(" GradientX %d, GradientY %d, GranularX %d, GranularY %d, \n X %f, Y %f, Position %s"),
					// 	GradientX, GradientY, GranularX, GranularY, X, Y, *Position.ToString());
				}
			}
		}
	}
}

// Generate Blocks  ////////////////////////////////////////////////////////////////////////

void UWorldGenerator::SpawnBlockColumn(int ChunkIndex, int X, int Y)
{
	FChunk& Chunk = Chunks[ChunkIndex];
	SpawnBlockColumn(Chunk, X, Y, ChunkIndex);
}

void UWorldGenerator::SpawnBlockColumn(FChunk* Chunk, int X, int Y, int ChunkIndex)
{
	if(ChunkIndex < 0)
	{
		ChunkIndex = FChunkLib::FindChunkIndex(Chunks, Chunk->X, Chunk->Y);
	}
	SpawnBlockColumn(*Chunk, X, Y, ChunkIndex);
}

void UWorldGenerator::SpawnBlockColumn(FChunk& Chunk, int X, int Y, int ChunkIndex)
{
	if(ChunkIndex < 0)
	{
		ChunkIndex = FChunkLib::FindChunkIndex(Chunks, Chunk.X, Chunk.Y);
	}
	FVector Position = Chunk.BlocksTopCoordinates[X].Array[Y];
	
	TArray<TSubclassOf<ABlock>>& BlockTypes = GameMode->BlockTypes;
	
	// Load Block types
	const TSubclassOf<ABlock> SnowBlock = BlockTypes[0];
	const TSubclassOf<ABlock> DirtBlock = BlockTypes[1];
	const TSubclassOf<ABlock> RockBlock = BlockTypes[2];
	const TSubclassOf<ABlock> BrickBlock = BlockTypes[3];

	// UE_LOG(LogWorldGen, Log, TEXT("Generating column with coords: %s"), *(Position/100).ToString());

	int CurrentDepth = 0;
	const int GenerateToDepth = GameMode->GenerationDepth;
	const double OriginalZ = Position.Z;
	// Spawn a column; Multiply coordinates by 100, because 1 in Unreal is cm 
	while (Position.Z > -PerlinHeightMultiplierCm && CurrentDepth < GenerateToDepth)
	{
		// Do not spawn blocks that the player has mined
		FIntVector IntPosition = FChunkLib::RoundVectorToInt(Position);
		if (ChunkBlocks[ChunkIndex].Mined.ContainsByPredicate([IntPosition](FIntVector Test)
		{
			return (IntPosition.X == Test.X && IntPosition.Y == Test.Y && IntPosition.Z == Test.Z);
		}))
		{
			Position.Z -= 100.;
			CurrentDepth++;
			continue;
		}
		
		// Assign Brick so that we notice errors
		TSubclassOf<ABlock> BlockType = BrickBlock;
		// Put snow high						// But under surface leave space for Rock
		if (Position.Z >= SnowHeightCm && Position.Z > OriginalZ - HardenDepthCm)
		{
			BlockType = SnowBlock;
			// A bit below snow height and deep under surface put Rock
		} else if (Position.Z >= SnowHeightCm - HardenDepthCm ||
			Position.Z < OriginalZ - HardenDepthCm) // Under surface place Rock
		{
			BlockType = RockBlock;
		}
		// Otherwise Dirt
		else
		{
			BlockType = DirtBlock;
		}
		ABlock* Block = GetWorld()->SpawnActor<ABlock>(BlockType, Position, FRotator(0, 0, 0));
		AddBlockToChunk(Block, ChunkIndex, false);
		
		Position.Z -= 100.;
		CurrentDepth++;
	}
}

void UWorldGenerator::SpawnChunkBlocks(FChunk* Chunk)
{
	SpawnChunkBlocks(*Chunk);
}

void UWorldGenerator::SpawnChunkBlocks(FChunk& Chunk)
{
	UE_LOG(LogWorldGen, Log, TEXT("    with coords (%d %d)"), Chunk.X, Chunk.Y);
	const int ChunkIndex = FChunkLib::FindChunkIndex(Chunks, Chunk.X, Chunk.Y);
	
	// Spawn default world blocks from generation
	for (int X = 0; X < Chunk.BlocksTopCoordinates.Num(); X++)
	{
		for (int Y = 0; Y < Chunk.BlocksTopCoordinates.Num(); Y++)
		{
			SpawnBlockColumn(Chunk, X, Y, ChunkIndex);
		}
	}

	for (FUserBlockData BlockData : ChunkBlocks[ChunkIndex].UserData)
	{
		ABlock* Block = GetWorld()->SpawnActor<ABlock>(BlockData.BlockType,
			FVector(BlockData.Position), FRotator(0, 0, 0));
		AddBlockToChunk(Block, ChunkIndex, true, true);
	}
}

// Generate Other //////////////////////////////////////////////////////////////////////////

void UWorldGenerator::GenerateWorld()
{
	if (!ValidateInputsOfWorldGeneration())
	{
		return;
	}
	InitMembers();

	const FVector PlayerPosition = UGameplayStatics::GetPlayerController(GetWorld(),0)->GetPawn()->GetActorLocation();
	UE_LOG(LogWorldGen, Log, TEXT("///////////// Generating world at %s"), *PlayerPosition.ToString());

	AdjustWorldToPlayerPosition(PlayerPosition);
}

void UWorldGenerator::AdjustWorldToPlayerPosition(const FVector& PlayerPosition)
{
	
	const FIntVector2 PlayerChunkCoords = FChunkLib::GetChunkCoordsFromPosition(PlayerPosition, ChunkOffsetCm);
	UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - Player chunk (%d %d)"), PlayerChunkCoords.X, PlayerChunkCoords.Y);

	// Generate chunks in wider neighborhood
	TArray<FIntVector2> ChunksToGenerate = ComputeSurroundingChunkCoords(PlayerChunkCoords, RADIUS_TO_GENERATE);
	TArray<FChunk*> GeneratedChunks = AllocateChunksFromStartCoords(ChunksToGenerate);
	GenerateChunks(GeneratedChunks);

	// TODO Add smoothing here?

	// We want only blocks of near chunks - Determine their coordinates
	TArray<FIntVector2> ChunksToSpawn = ComputeSurroundingChunkCoords(PlayerChunkCoords, RADIUS_TO_SPAWN);
	TArray<FChunk*> SpawnedChunks;
	
	// Spawn blocks of chunks that have been deleted earlier and delete actors of far away chunks
	for (int Index = 0; Index < Chunks.Num(); Index++)
	{
		FChunk& Chunk = Chunks[Index];
		UE_LOG(LogWorldGen, Log, TEXT("    Chunks I %d; ChunkBlocks I %d; Chunks coord (%d %d); ChunkBlocksCoord (%d %d); World blocks %d"),
			Index, Index, Chunk.X, Chunk.Y, ChunkBlocks[Index].X, ChunkBlocks[Index].Y, ChunkBlocks[Index].WorldBlocks.Num());
		if (ChunksToSpawn.ContainsByPredicate([Chunk](FIntVector2 Test)
		{
			return Chunk.X == Test.X && Chunk.Y == Test.Y;
		}))
		{
			// Spawn blocks of older chunks that have been destroyed already and also new chunks
			if (ChunkBlocks[Index].WorldBlocks.Num() == 0)
			{
				UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - Adding to spawn (%d %d)"), Chunk.X, Chunk.Y);
				SpawnedChunks.Add(&Chunk);
			}
			else
			{
				UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - Already spawned (%d %d) with %d blocks"), Chunk.X, Chunk.Y, ChunkBlocks[Index].WorldBlocks.Num());
			}
		}
		else 
		{
			if (ChunkBlocks[Index].WorldBlocks.Num() == 0)
			{
				UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - deleting (%d %d) Empty"), ChunkBlocks[Index].X, ChunkBlocks[Index].Y);
			}
			else
			{
				UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - deleting (%d %d) with %d"), ChunkBlocks[Index].X, ChunkBlocks[Index].Y, ChunkBlocks[Index].WorldBlocks.Num());
			}
			// And delete actors of far away chunks
			// UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - deleting (%d %d)"), Chunk.X, Chunk.Y);
			DeleteChunkBlocks(Index, ChunkBlocks);
		}
	}
	UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - Allocated %d chunks; %d will spawn"), Chunks.Num(), SpawnedChunks.Num() );
	SpawnChunks(SpawnedChunks);
}

void UWorldGenerator::SpawnLoadedWorld()
{
	const FIntVector2 PlayerChunkCoords = FChunkLib::GetChunkCoordsFromPosition(PlayerTransform.GetLocation(), ChunkOffsetCm);
	UE_LOG(LogWorldGen, Log, TEXT("  Spawn Loaded World - Player chunk (%d %d)"), PlayerChunkCoords.X, PlayerChunkCoords.Y);

	// All needed chunks are generated. Just spawn blocks
	// We want only blocks of near chunks - Determine their coordinates
	TArray<FIntVector2> ChunksToSpawn = ComputeSurroundingChunkCoords(PlayerChunkCoords, RADIUS_TO_SPAWN);
	TArray<FChunk*> SpawnedChunks;

	for (FIntVector2 ChunkCoords : ChunksToSpawn)
	{
		SpawnedChunks.Add(FChunkLib::FindChunk(Chunks, ChunkCoords));
	}
	
	UE_LOG(LogWorldGen, Log, TEXT("  Adjust World - %d chunks will spawn"), SpawnedChunks.Num() );
	SpawnChunks(SpawnedChunks);
}

template<typename T>
void UWorldGenerator::GenerateChunks(TArray<T>& Array)
{
	ParallelFor(Array.Num(), [&](int I)
	{
		UE_LOG(LogWorldGen, Log, TEXT("  Computing Chunk %d Gradients"), I);
		FChunkLib::ComputeGradients(Array[I]);
	});

	ParallelFor(Array.Num(), [&](int I)
	{
		UE_LOG(LogWorldGen, Log, TEXT("  Computing Chunk %d Smooth values"), I);
		FChunkLib::ComputeGradientSmoothingData(*this, Array[I]);
	});

	ParallelFor(Array.Num(), [&](int I)
	{
		UE_LOG(LogWorldGen, Log, TEXT("  Computing Chunk %d Block heights"), I);
		ComputeBlockHeights(*this, Array[I], FChunkLib::EIM_Cubic);
	});
}

template<typename T>
void UWorldGenerator::SpawnChunks(TArray<T>& Array)
{
	// Unfortunately Actors need to be spawned in Game thread
	for (int I = 0; I < Array.Num(); I++)
	{
		UE_LOG(LogWorldGen, Log, TEXT("  Spawning Chunk %d Blocks"), I);
		SpawnChunkBlocks(Array[I]);
	}
}

template<typename T>
void UWorldGenerator::GenerateAndSpawnChunks(TArray<T>& Array)
{
	GenerateChunks(Array);
	SpawnChunks(Array);
}

TArray<FIntVector2> UWorldGenerator::ComputeSurroundingChunkCoords(const FIntVector2& StartCoords, int Radius) const
{
	TArray<FIntVector2> Result;
	TArray<int> Offsets;
	
	for (int I = -Radius; I < Radius + 1; I++)
	{
		Offsets.Add(I * ChunkOffsetCm);
	}

	for (const int X : Offsets)
	{
		for (const int Y : Offsets)
		{
			Result.Add(FIntVector2(X + StartCoords.X, Y + StartCoords.Y));
		}
	}
	return Result;
}

TArray<FChunk*> UWorldGenerator::AllocateChunksFromStartCoords(TArray<FIntVector2>& StartCoords)
{
	TArray<FChunk*> AddedChunks;
	
	UE_LOG(LogWorldGen, Log, TEXT(" Pre purge %d"), Chunks.Num());
	// Remove from StartCoords coords of chunks that are already created 
	// But skip during initial world generation, since (0,0) chunk would be skipped due to Chunks.SetNum() allocation
	if(Chunks.Num() != 0)
	{
		for (FChunk& Chunk : Chunks)
		{
			StartCoords.RemoveAll([Chunk](FIntVector2 Test)
			{
				return Test.X == Chunk.X && Test.Y == Chunk.Y;
			});
		}
	}
	UE_LOG(LogWorldGen, Log, TEXT(" Post purge %d"), Chunks.Num());
	// Allocate memory so that the array is not moved during processing and out pointer are not messed up
	int Index = Chunks.Num();
	Chunks.SetNum(Chunks.Num() + StartCoords.Num(), false);
	
	for (const FIntVector2 StartCoord : StartCoords)
	{
		Chunks[Index] = FChunk(StartCoord.X, StartCoord.Y, ChunkGranularity);
		AddedChunks.Add(&Chunks[Index]);
		// Also create new object to store Block Actors and user modifications to the world
		ChunkBlocks.Add(FChunkBlocks(StartCoord.X, StartCoord.Y));
		Index++;
	}
	
	return AddedChunks;
}

// Block Handling ////////////////////////////////////////////////////////////////////////////////////////

void UWorldGenerator::AddBlockToChunk(ABlock* Block, const bool bUser)
{
	if(!Block)
	{
		return;
	}
	const FIntVector2 ChunkCoords = FChunkLib::GetChunkCoordsFromPosition(Block->GetActorLocation(), ChunkOffsetCm);
	AddBlockToChunk(Block, ChunkCoords, bUser);
}

void UWorldGenerator::AddBlockToChunk(ABlock* Block, const FIntVector2 ChunkCoords, const bool bUser)
{
	if(!Block)
	{
		return;
	}
	const int Index = FChunkLib::FindChunkIndex(Chunks, ChunkCoords);
	AddBlockToChunk(Block, Index, bUser);
}

void UWorldGenerator::AddBlockToChunk(ABlock* Block, const int ChunkIndex, const bool bUser, const bool bRespawn)
{
	if(!Block)
	{
		return;
	}
	if (bUser)
	{
		ChunkBlocks[ChunkIndex].UserBlocks.Add(Block);
		// If bRespawn then the UserData is already there
		if (!bRespawn)
		{
			
			ChunkBlocks[ChunkIndex].UserData.Add({
				FChunkLib::RoundVectorToInt(Block->GetActorLocation()),
				GetBlockClass(Block)
			});
		}
	}
	else
	{
		ChunkBlocks[ChunkIndex].WorldBlocks.Add(Block);
	}
	
}

void UWorldGenerator::MineBlockFromChunk(ABlock* Block)
{
	if(!Block)
	{
		return;
	}
	const FIntVector IntPosition(FChunkLib::RoundVectorToInt(Block->GetActorLocation()));
	const FIntVector2 ChunkCoords = FChunkLib::GetChunkCoordsFromPosition(Block->GetActorLocation(), ChunkOffsetCm);
	const int Index = FChunkLib::FindChunkIndex(Chunks, ChunkCoords);
	
	ChunkBlocks[Index].Mined.Add(IntPosition);
	ChunkBlocks[Index].UserBlocks.Remove(Block);
	ChunkBlocks[Index].UserData.RemoveAll([Block](const FUserBlockData& Test)
	{
		return (Block->IsA(Test.BlockType) && FChunkLib::RoundVectorToInt(Block->GetActorLocation()) == Test.Position);
	});	
}

void UWorldGenerator::DeleteChunkBlocks(const FIntVector2 ChunkCoords)
{
	const int Index = FChunkLib::FindChunkIndex(Chunks, ChunkCoords);
	DeleteChunkBlocks(Index, ChunkBlocks);
}

void UWorldGenerator::DeleteChunkBlocks(const int ChunkIndex, TArray<FChunkBlocks>& InChunkBlocks)
{
	for (ABlock* Block : InChunkBlocks[ChunkIndex].WorldBlocks)
	{
		if(!Block)
		{
			continue;
		}
		Block->Destroy();
	}
	InChunkBlocks[ChunkIndex].WorldBlocks.Empty();
	for (ABlock* Block : InChunkBlocks[ChunkIndex].UserBlocks)
	{
		if(!Block)
		{
			continue;
		}
		Block->Destroy();
	}
	InChunkBlocks[ChunkIndex].UserBlocks.Empty();
}

// Save / Load ////////////////////////////////////////////////////////////////////////////////////////

void UWorldGenerator::SaveWorld(const AFPS_CppCharacter& Player)
{
	PlayerTransform = Player.GetTransform();

	// If not yet saved, create a new save object
	if(!SavedData)
	{
		SavedData = Cast<USavedWorld>(UGameplayStatics::CreateSaveGameObject(USavedWorld::StaticClass()));
	}

	// Fill the object with data we want to save
	SavedData->BlockTypeCount = GameMode->BlockTypes.Num();
	SavedData->ChunkGranularity = ChunkGranularity;
	SavedData->PerlinHeightMultiplier = PerlinHeightMultiplier;
	SavedData->SnowHeight = GameMode->SnowHeight;
	SavedData->HardenDepth = GameMode->HardenDepth;
	SavedData->PlayerTransform = Player.GetTransform();
	SavedData->Chunks = Chunks;
	SavedData->ChunkBlocks.SetNum(ChunkBlocks.Num(), false);
	for (int Index = 0; Index < ChunkBlocks.Num(); Index++)
	{
		SavedData->ChunkBlocks[Index].X = ChunkBlocks[Index].X;
		SavedData->ChunkBlocks[Index].Y = ChunkBlocks[Index].Y;
		SavedData->ChunkBlocks[Index].Mined = ChunkBlocks[Index].Mined;
		SavedData->ChunkBlocks[Index].UserData = ChunkBlocks[Index].UserData;
	}
	
	UE_LOG(LogWorldGen, Log, TEXT(" SavedData - %d %d %d"), SavedData->BlockTypeCount, SavedData->ChunkGranularity, SavedData->Chunks.Num());

	// Save the game to the specified slot
	if (UGameplayStatics::SaveGameToSlot(SavedData, SaveSlotName, 0))
	{
		UE_LOG(LogWorldGen, Log, TEXT(" SaveWorld - Success"));
	}
	else
	{
		UE_LOG(LogWorldGen, Error, TEXT(" SaveWorld - Failed"));
	}
}

void UWorldGenerator::LoadWorld(AFPS_CppCharacter& Player)
{
	SavedData = Cast<USavedWorld>(UGameplayStatics::LoadGameFromSlot(SaveSlotName, 0));
	
	if (!SavedData)
	{
		UE_LOG(LogWorldGen, Error, TEXT(" LoadWorld - Failed during load"));
		return;
	}

	UE_LOG(LogWorldGen, Log, TEXT(" LoadedData - %d %d %d"), SavedData->BlockTypeCount, SavedData->ChunkGranularity, SavedData->Chunks.Num());
	
	if (SavedData->BlockTypeCount > GameMode->BlockTypes.Num() ||
		SavedData->ChunkGranularity != ChunkGranularity ||
		SavedData->PerlinHeightMultiplier != PerlinHeightMultiplier)
	{
		UE_LOG(LogWorldGen, Error, TEXT(" LoadWorld - Crutial world generations settings differ from saved state. Unable to generate world."));
		return;
	}

	if (SavedData->BlockTypeCount != GameMode->BlockTypes.Num() ||
		SavedData->ChunkGranularity != ChunkGranularity ||
		SavedData->PerlinHeightMultiplier != PerlinHeightMultiplier ||
		SavedData->SnowHeight != GameMode->SnowHeight ||
		SavedData->HardenDepth != GameMode->HardenDepth)
	{
		UE_LOG(LogWorldGen, Warning, TEXT(" LoadWorld - Some world generation parameters differ from saved state.\n"
			       "The world will probably be different in some way.\n"
			       "World generation parameters in GameMode will be overriten by those in saved state."));
	}

	UE_LOG(LogWorldGen, Log, TEXT(" LoadWorld - Saved state world generation parameters seem OK"));

	GameMode->ChunkGranularity = SavedData->ChunkGranularity;
	GameMode->PerlinHeightMultiplier = SavedData->PerlinHeightMultiplier;
	GameMode->SnowHeight = SavedData->SnowHeight;
	GameMode->HardenDepth = SavedData->HardenDepth;
	InitMembers();

	PlayerTransform = SavedData->PlayerTransform;
	Player.SetActorTransform(SavedData->PlayerTransform);
	
	Chunks = SavedData->Chunks;
	// Delete current blocks
	for (int Index = 0; Index < ChunkBlocks.Num(); Index++)
	{
		DeleteChunkBlocks(Index, ChunkBlocks);
	}
	ChunkBlocks = SavedData->ChunkBlocks;
	
	SpawnLoadedWorld();
}

