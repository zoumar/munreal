// Copyright Epic Games, Inc. All Rights Reserved.

#include "FPS_CppGameMode.h"

#include "WorldExtendDetector.h"
#include "UObject/ConstructorHelpers.h"

AFPS_CppGameMode::AFPS_CppGameMode()
	: Super()
{
	WorldGenerator = CreateDefaultSubobject<UWorldGenerator>(TEXT("WorldGenerator"));
	AddOwnedComponent(WorldGenerator);

	// For CreateDefaultSubobject to work some other handling of parenting and moving is needed. Spawn actor for now
	// WorldExtendDetector = CreateDefaultSubobject<AWorldExtendDetector>(TEXT("WorldExtendDetector"));
	
}

void AFPS_CppGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	// Make sure we don't kill player on accident
	GetWorld()->GetWorldSettings()->KillZ = -(PerlinHeightMultiplier * 100);

	WorldExtendDetector = GetWorld()->SpawnActor<AWorldExtendDetector>(AWorldExtendDetector::StaticClass(),
			FVector(0,0,0), FRotator(0,0,0));
	WorldExtendDetector->InitMembers(*this);
	WorldExtendDetector->SetupBoundaries();

	WorldGenerator->GenerateWorld();
}
