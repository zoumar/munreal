// Fill out your copyright notice in the Description page of Project Settings.


#include "Chunk.h"

FChunk::FChunk(): X(0), Y(0)
{
}

// Allocate all memory that this FChunk class will need
FChunk::FChunk(int InX, int InY, int Granularity)
{
	X = InX;
	Y = InY;
	
	Gradients.SetNum(CHUNK_GRADIENT_ARRAY_SIZE + 1, false);
	for (FVector2fArray& Row : Gradients)
	{
		Row.Array.SetNum(CHUNK_GRADIENT_ARRAY_SIZE + 1, false);
	}

	Granularity *= CHUNK_GRADIENT_ARRAY_SIZE;
	BlocksTopCoordinates.SetNum(Granularity, false);
	for (FVectorArray& Row : BlocksTopCoordinates)
	{
		Row.Array.SetNum(Granularity, false);
	}
}

FChunk::~FChunk() {}
